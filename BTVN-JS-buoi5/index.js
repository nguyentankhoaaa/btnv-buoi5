// Bài 1:
function tinhDiem(){
    var diemChuan = document.getElementById("txt-diem-chuan").value*1;

    var diem1 = document.getElementById("txt-diem-1").value*1;
    var diem2 = document.getElementById("txt-diem-2").value*1;
    var diem3 = document.getElementById("txt-diem-3").value*1;

    var khuVuc = document.getElementById("txt-khu-vuc").value;
    var doiTuong = document.getElementById("txt-doi-tuong").value;
    
    var diemDoiTuong,diemKhuVuc;

    
    if(diem1 == 0 || diem2 == 0 || diem3 == 0){
        document.getElementById("result").innerHTML=`<h2 class="text-danger my-3">Bạn đã rớt tuyển !</h2>`
    }
    else{
        switch(khuVuc){
            case "kv":{
                diemKhuVuc = 0;
                break;
            }
            case "kvA":{
                diemKhuVuc = 2;
                break;
            }
            case "kvB":{
                diemKhuVuc = 1;
                break;
            }
            case "kvC":{
                diemKhuVuc = 0.5;
                break;
            }
        }
        switch(doiTuong){
            case "dt":{
                diemDoiTuong = 0;
                break;
            }
            case "dt1":{
                diemDoiTuong = 2.5;
                break;
            }
            case "dt2":{
                diemDoiTuong = 1.5;
                break;
            }
            case "dt3":{
                diemDoiTuong = 1;
                break;
            }
        }
        var tongDiem = diem1 + diem2 + diem3 + diemDoiTuong + diemKhuVuc;
        if( tongDiem < diemChuan){
            document.getElementById("result").innerHTML=`<h2 class="text-danger my-3">Bạn đã rớt tuyển!
            tổng điểm của bạn là : ${tongDiem}
            </h2>`
        }
        else{
            document.getElementById("result").innerHTML=`<h2 class="text-danger my-3">Bạn đã trúng tuyển!
            tổng điểm của bạn là : ${tongDiem}
            </h2>`
      }

 
    }
  
    }




    // Bài 2:
    function tienDien(){
        var ten = document.getElementById("txt-ten").value;
        var soKw = document.getElementById("txt-so-kw").value*1;
       
        var soTienMoiKw,tinhTienKw;
     
     if(soKw <= 50){
        soTienMoiKw = 500;
        tinhTienKw =  soTienMoiKw * soKw;
     }   
     else if(soKw > 50 && soKw <= 100){
        soTienMoiKw = 650;
         tinhTienKw = (50 * 500) + ((soKw-50)*650);
     }
    else if(soKw > 100 && soKw <= 200){
        soTienMoiKw = 850;
        tinhTienKw = (50 * 500) + (50*650) + ((soKw-100)*850);
     }
    else if(soKw > 200 && soKw <= 350){
        soTienMoiKw = 1.100;
        tinhTienKw = (50 * 500) + (50*650) +(100 * 850) +((soKw-200)*1.100);
     }
     else{
        soTienMoiKw = 1300;
        tinhTienKw = (50 * 500) + (50*650) +(100 * 850) +(150 * 1100)+((soKw-300)*1300);
     }
     document.getElementById("result2").innerHTML = `<h2  class="text-success my-3">
     Tên:${ten} , Tiền điện:${tinhTienKw}VNĐ    
     </h2>`;
    }